package local.sealz.sample.service;

import local.sealz.sample.entity.Emp;

public class EmpService extends AbstractService<Emp> {

    public Emp findById(Long id) {
        return select().id(id).getSingleResult();
    }
}