package local.sealz.sample.service;

import local.sealz.sample.entity.Dept;

public class DeptService extends AbstractService<Dept> {

    public Dept findById(Long id) {
        return select().id(id).getSingleResult();
    }
}